import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import ProductRouter from "./routers/ProductRouter.js";
import CustomerRouter from "./routers/CustomerRouter.js";
import OrderRouter from "./routers/OrderRouter.js";
import OrderDetailRouter from "./routers/OrderDetailRouter.js";

import mongoose from "mongoose";
import bodyParser from "body-parser";
dotenv.config();

const app = express();

const PORT = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

const URI = process.env.MONGODB_URL;

mongoose
  .connect(URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log("connect to successfully");
    app.listen(PORT, () => {
      console.log("running is port: " + PORT);
    });
  })
  .catch((err) => {
    console.log("connect failed. Error: " + err.message);
  });

app.use("/", ProductRouter);
app.use("/", CustomerRouter);
app.use("/", OrderRouter);
app.use("/", OrderDetailRouter);
