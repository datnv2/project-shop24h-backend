import express from "express";
import {
  createOrder,
  getAllOrder,
  getOrderByCustomerId,
  updateOrderByOrderId,
  deleteOrderByOrderId,
  getOrderByOrderId,
} from "../controllers/OrderController.js";

const router = express.Router();

router.get("/order", getAllOrder);
router.get("/order/:customerid", getOrderByCustomerId);
router.get("/order-detail/:orderid", getOrderByOrderId);
router.post("/order", createOrder);

router.put("/order/:orderid", updateOrderByOrderId);

router.delete("/order/:orderid", deleteOrderByOrderId);

export default router;
