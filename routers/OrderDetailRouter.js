import express from "express";

import {
  createOrderDetail,
  getAllOrderDetail,
  getOrderDetailByOrderDetailId,
  updateOrderDetailByOrderDetailId,
  deleteOrderDetail,
  getOrderDetailByOrderId,
  getOrderDetailById,
} from "../controllers/OrderDetailController.js";

const router = express.Router();

router.get("/order-detail", getAllOrderDetail);

router.post("/order-detail", createOrderDetail);

router.get("/order-detail/:orderdetailid", getOrderDetailByOrderDetailId);

router.put("/order-detail/:orderdetailid", updateOrderDetailByOrderDetailId);

router.delete("/order-detail/:orderdetailid", deleteOrderDetail);

router.get("/order-detail-orderid/:order", getOrderDetailByOrderId);
router.get("/order-detail-id/:orderid", getOrderDetailById);

export default router;
