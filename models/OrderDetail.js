import mongoose from "mongoose";

const { Schema } = mongoose;

const orderDetail = new Schema({
  orderId: { type: Schema.Types.ObjectId },
  productId: { type: Schema.Types.ObjectId },
  quantity: { type: Number },
  priceEach: { type: Number },
});

export const OrderDetail = mongoose.model("OrderDetail", orderDetail);
